// lodash
window._ = require('lodash');

// popper
window.Popper = require('popper.js').default;

// jquery && bootstrap
try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

// axios
window.axios = require('axios');

// vue
window.Vue = require('vue');

// app
const app = new Vue({
    el: '#app',
    data() {
        return {
            message: 'Hello!',
        }
    }
});

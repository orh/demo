## 开发

1. 拉取仓库

```
git clone git@gitee.com:orh/demo.git
```

2. 安装依赖包

```
yarn
```

3. 编译命令

```
npm run dev
```

## 结构

```
.
├── .editorconfig # 代码风格文件
├── .gitignore # git 忽略文件
├── .yarnrc # 依赖包国内镜像
├── package.json # 依赖包配置文件
├── public 
│   ├── css # 编译后 css
│   │   └── app.css
│   ├── index.html # 示例文件
│   ├── js
│   │   └── app.js # 编译后 js
│   └── mix-manifest.json
├── readme.md # 说明文档
├── src # 源代码
│   ├── js # js 目录
│   │   └── app.js
│   └── sass # sass 目录
│       ├── app.scss # 主体样式
│       └── _variables.scss # 变量定义
├── webpack.mix.js # webpack 文件
└── yarn.lock # 依赖包锁文件
```
